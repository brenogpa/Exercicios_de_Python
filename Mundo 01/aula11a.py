#Style    Text    Back#
#\033[0;33;44m

#Style : 0 = none    1 = Negrito   4 = Sublinhar    7 = Negativo

#Text : 30 = Branco  31 = Vermelho  32 = Verde  33 = Amarelo  34 = Azul  35 = Magenta  36 = Ciano  37 = Cinza

#Back : 40 = Branco  41 = Vermelho  42 = Verde  43 = Amarelo  44 = Azul  45 = Magenta  46 = Ciano  47 = Cinza

'''
\033[0;30;41m
\033[4;33;44m
\033[1;35;43m
\033[30;42m
\033[m
\033[7;30m
'''
print('\033[0;30;41mTeste\033[m')
print('\033[4;33;44mTeste\033[m')
print('\033[1;35;43mTeste\033[m')
print('\033[30;42mTeste\033[m')
print('\033[mTeste')
print('\033[7;30mTeste\33[m')