#crie um programa que leia um número inteiro e mostra na tela se ele é PAR ou ÍMPAR.
n = int(input('Insira um número inteiro: '))
x = n % 2
if x == 1:
    print('O número {} é IMPAR'.format(n))
else:
    print('O número {} é PAR'.format(n))
