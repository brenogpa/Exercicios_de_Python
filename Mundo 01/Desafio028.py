import random
nome = str(input('Por favor, insira seu nome: ')).title().strip()
rdm = random.randint(0, 5)
num = int(input('Olá, {}! O computador escolheu um número entre 0 e 5... tente adivinhar qual:'.format(nome)))
if num == rdm:
    print('Você acertou, o computador escolheu o número {}'.format(rdm))
else:
    print('Você errou, o computador escolheu o número {}'.format(rdm))