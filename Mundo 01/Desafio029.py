v = float(input('Qual a velocidade do seu carro?: '))
if v >= 81:
    print('Sua velocidade é de {:.1f} km/h e está acima do limite.'.format(v))
    print('Você deverá pagar uma multa de: R${:.2f}'.format((v - 80) * 7))
else:
    print('Sua velocidade é de {:.1f} km/h e está dentro do limite.'.format(v))