print('----Desafio 006----')

n1 = int(input('Digite um número:'))
dobro = n1 * 2
triplo = n1 * 3
raiz = n1 ** (1/2)

print(' Dobro: {} \n Triplo: {} \n Raiz Quadrada: {:.2f}'.format(dobro, triplo, raiz))