import math
a = float(input('Digite o ângulo que você deseja: '))
print('O ângulo de {} tem o SENO de {:.2f}\nO ângulo de {} tem o COSSENO de {:.2f}\nO ângulo de {} tem a TANGENTE de {:.2f}'.format(a, math.sin(math.radians(a)), a, math.cos(math.radians(a)), a, math.tan(math.radians(a))))