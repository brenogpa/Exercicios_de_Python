valor = float(input('Valor do produto: R$'))
cond1 = valor - (valor * 10 / 100)
cond2 = valor - (valor * 5 / 100)
cond3 = valor / 2
cond4 = valor + (valor * 20 / 100)
forma = int(input('''
[1] À Vista/Cheque
[2] À Vista no Cartão
[3] Até 2x no Cartão
[4] 3x ou mais no Cartão
Qual a forma de pagamento?: '''))

if forma == 1:
    print('Valor final da compra: R${:.2f} com os 10% de desconto!'.format(cond1))
elif forma == 2:
    print('Valor final da compra: R${:.2f} com os 5% de desconto!'.format(cond2))
elif forma == 3:
    print('Valor final da compra: 2 parcelas de R${:.2f} no cartão. Total de R${:.2f}!'.format(cond3, valor))
elif forma == 4:
    parcelas = int(input('Quantas Parcelas?: '))
    print('Valor final da compra: {} parcelas de R${:.2f}. Total de R${:.2f}!'.format(parcelas, cond4 / parcelas, cond4))
else:
    print('\033[1;31mOPÇÃO INVÁLIDA\033[m')
