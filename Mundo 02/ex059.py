import time
n1 = float(input('Primeiro valor: '))
n2 = float(input('Segundo valor: '))
opção = 0
d =  '*-' * 10
while opção != 5:
    print('''{}
    [1] Somar
    [2] Multiplicar
    [3] Maior
    [4] Novos Números
    [5] Sair do Programa
    '''.format(d))
    opção = int(input('>>>>> Qual a sua opção?: '))
    if opção == 1:
        print('{} + {} = {}.'.format(n1,n2,n1+n2))
    elif opção == 2:
        print('{} x {} = {}.'.format(n1,n2,n1*n2))
    elif opção == 3:
        if n1 > n2:
            print('{} > {}.'.format(n1,n2))
        elif n1 < n2:
            print('{} > {}'.format(n2,n1))
        else:
            print('São iguais.')
    elif opção == 4:
        n1 = float(input('Primeiro valor: '))
        n2 = float(input('Segundo valor: '))
    elif opção <= 0 and opção > 5:
        print('\033[1;31mOPÇÃO INVÁLIDA!\033[m Tente novamente.')
print('Finalizando...')
time.sleep(2)
print('''{}
Fim do Programa, volte sempre!'''.format(d))