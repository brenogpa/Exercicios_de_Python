# ==============01==============
import math
n = int(input('Insira um número para calcular seu fatorial: '))
print('O fatorial de {} é {}'.format(n, math.factorial(n)))
#===============02==============
n = int(input('Digite um número para calcular seu Fatorial: '))
f = 1
print('{}! = '.format(n), end='')
while n > 0:
    print('{}'.format(n), end='')
    print(' x ' if n > 1 else ' = ', end='')
    f *= n
    n -= 1
print('{}'.format(f))
#===============03===============
n = int(input('Insira um numero para calcular seu Fatorial: '))
f = 1
print('{}! '.format(n),end='')
for n in range(n, 0 -1, -1):
    if n != 0:
        print('{}'.format(n), end='')
        print(' x ' if n > 1 else ' = ', end='')
        f *= n
print(f)