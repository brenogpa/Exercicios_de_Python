#Estrutura de Repetição (laços)

for b in range(1,6):
    print(b)

print('-------')

for c in range(0, 7, 2):
    print(c)

print('--------')

for a in range(6 , -1, -1):
    print(a)

print('---------')

for d in range(0,6):
    print('oi')

print('----------')

n = int(input('Digite um numero:'))
for p in range(1,n+1):
    print(p)

print('-----------')
s = 0
for n in range(0 , 3):
    n = int(input('Digite um valor: '))
    s = s + n
print('{}'.format(s))
