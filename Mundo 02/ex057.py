sexo = str(input('Informe seu sexo [M/F/O]: ')).strip().upper()[0]
while sexo not in 'MFO':
    print('\033[1;31mERRO\033[1m - Dados inválidos!\033[m')
    sexo = str(input('Informe seu sexo [M/F/O]: ')).strip().upper()[0]
if sexo == 'M':
    print('Sexo Masculino!')
elif sexo == 'F':
    print('Sexo Feminino!')
else:
    outro = str(input('Por favor, especifique: ')).strip().capitalize()
    print(outro,'!')