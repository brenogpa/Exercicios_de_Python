while True:
    n = int(input('Insira um número (negativo p/ encerrar o programa): '))
    if n < 0:
        break
    print('*-' * 10)
    for r in range(1,11):
        print(f'{n} x {r} = {n*r}')
    print('*-' * 10)
print('TABUADA ENCERRADA')