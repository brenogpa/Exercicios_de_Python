choice = ''
total = homens = mulheres = 0
while choice != 'N':
    print('-------------------\nCADASTRE UMA PESSOA\n-------------------')
    idade = int(input('Idade: '))
    if idade >= 18:
        total += 1
    sexo = str(input('Sexo [M/F]: ')).strip().upper()
    if sexo != 'M' and sexo != 'F':
        print('\033[1;31mERRO\033[m')
        sexo = str(input('Sexo [M/F]: ')).strip().upper()
    if  sexo == 'M':
        homens += 1
    choice = str(input('Quer continuar? [S/N]: ')).strip().upper()
    if sexo == 'F' and idade <= 20:
        mulheres += 1
    if choice != 'S' and choice !='N':
        print('\033[1;31mERRO\033[m')
        choice = str(input('Quer continuar? [S/N]: ')).strip().upper()
print(f'Pessoas com mais de 18 anos: {total}\nHomens cadastrados: {homens}\nMulheres com menos de 20 anos: {mulheres}')