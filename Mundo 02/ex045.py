import time, random
itens = ('Pedra', 'Papel', 'Tesoura')
computador = random.randint(0, 2)
print('''Suas Opções:
[0] PEDRA
[1] PAPEL
[2] TESOURA''')
jogador = int(input('Qual é a sua jogada?: '))
print('JO')
time.sleep(1)
print('KEN')
time.sleep(1)
print('PO!!')
print('*=' * 11)
print('O computador jogou {}'.format(itens[computador]))
print('O jogador jogou {}'.format(itens[jogador]))
print('*=' * 11)
if jogador >= 0 and jogador <= 2:
    if jogador == 0: #JOGADOR JOGOU PEDRA
        if computador == 0:#COMPUTADOR JOGOU PEDRA
            print('EMPATE')
        elif computador == 1: #COMPUTADOR JOGOU PAPEL
            print('Computador VENCEU!')
        elif computador == 2: #COMPUTADOR JOGOU TESOURA
            print('Jogador VENCEU')
    if jogador == 1: #JOGADOR JOGOU PAPEL
        if computador == 0: #COMPUTADOR JOGOU PEDRA
            print('Jogador VENCEU!')
        elif computador == 1:
            print('EMPATE')
        elif computador == 2:
            print('Computador VENCEU!')
    if jogador == 2:#JOGADOR JOGOU TESOURA
        if computador == 0:
            print('Computador VENCEU!')
        elif computador == 1:
            print('Jogador VENCEU')
        elif computador == 2:
            print('EMPATE')
else:
    print('\033[1;31mERRO, TENTE NOVAMENTE\033[m')
