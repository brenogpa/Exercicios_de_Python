num1 = int(input('Digite o primeiro número: '))
num2 = int(input('Digite o segundo número: '))

if num1 > num2:
    print('O \033[1;31mprimeiro valor\033[m é \033[1;34mmaior\033[m')
elif num1 < num2:
    print('O \033[1;31msegundo valor\033[m  é \033[1;34mmaior\033[m')
else:
    print('\033[1;31mNão existe\033[m valor maior, os dois são \033[1:34miguais\033[m')
