n = int(input('Digite um número ( 999 para parar): '))
contador = 0
soma = 0
while n != 999:
    soma += n
    contador += 1
    n = int(input('Digite um número ( 999 para parar): '))
print('Foram digitados {} números e a soma entre eles é {}'.format(contador, soma))
