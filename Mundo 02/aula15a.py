n = s = 0
while True:
    n = int(input('Digite um número: '))
    if n == 999: # flag ( condição de parada )
        break
    s += n
print(f'A soma vale {s}')   # PYTHON 3.6+
print('A soma vale {}'.format(s))   # PYTHON 3
print('A soma vale %a' % (s))   # PYTHON 2
