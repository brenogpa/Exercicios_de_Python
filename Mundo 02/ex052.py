num = int(input('Digite um número: '))
total = 0
for n in range(1,num+1):
    if num % n == 0:
        print('\033[33m{}\033[m'.format(n),end=' ')
        total += 1
    else:
        print('\033[31m{}\033[m'.format(n),end=' ')
print('\nO número {} foi dividível {} vezes.'.format(num,total))
if total == 2:
    print('O número é primo')
else:
    print('O número não é primo')
