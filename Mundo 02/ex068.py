import random
PouI = ''
vezes = 0
while True:
    PouI = str(input('[P]ar ou [I]mpar?: ')).strip().upper()
    while True:
        if PouI != 'P' and PouI != 'I':
            PouI = str(input('[P]ar ou [I]mpar?: ')).strip().upper()
        else:
            break
    jogador = int(input('Escolha um valor entre 0 e 10: '))
    computador = random.randint(0,10)
    soma = jogador + computador
    vezes += 1
    print(f'Você escolheu {jogador} e o computador escolheu {computador}. Total de {soma}.')
    if soma % 2 == 0:
        if PouI == 'P':
            print('\033[1;32mVocê Venceu!\033[m\nVamos jogar denovo!')
        else:
            break
    else:
        if PouI == 'I':
            print('\033[1;32mVocê Venceu!\033[m\nVamos jogar denovo!')
        else:
            break
print(f'Você perdeu! Número de tentativas: \033[1;31m{vezes}\033[m\nPrograma encerrado...')