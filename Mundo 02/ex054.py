import datetime
atual = datetime.date.today().year
maior = 0
menor = 0
for n in range(1,8):
    ano = int(input('Ano em que a {}ª pessoa nasceu: '.format(n)))
    idade = atual - ano
    if idade >= 21:
        maior += 1
    else:
        menor += 1
print('Há {} menores e {} maiores de idade.'.format(menor, maior))