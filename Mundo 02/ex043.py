peso = float(input('Peso(Kg): '))
altura = float(input('Altura(m): '))
imc = peso / (altura ** 2)

print('Índice de massa corporal: {:.1f}! '.format(imc), end='')

if imc < 18.5:
    print('Abaixo do peso!')
elif imc >= 18.5 and imc < 25:
    print('Peso Ideal!')
elif imc >= 25 and imc < 30:
    print('Sobrepeso!')
elif imc >= 30 and imc < 40:
    print('Obesidade!')
else:
    print('Obesidade mórbida!')
