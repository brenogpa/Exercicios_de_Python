a = float(input('Primeiro segmento: '))
b = float(input('Segundo segmento: '))
c = float(input('Terceiro segmento: '))

if a < b + c and b < a + c and c < a + b:
    print('Os segmentos acima podem formar um triângulo ', end='')
    if a == b == c:
        print('EQUILÁTERO!')
    if a != b != c != a:
        print('ESCALENO!')
    if a == b != c or b == c != a or a == c != b:
        print('ISÓSCELES!')
else:
    print('Os segmentos acima NÃO podem formar um triângulo.')
    