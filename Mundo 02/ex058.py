import random
computador = random.randint(0,10)
print('Sou o computador...\nAcabei de pensar em um número de 0 a 10.\nSerá que você consegue adivinhar qual foi?')
palpite = int(input('Qual é seu palpite?: '))
npalpites = 1
while palpite != computador:
    npalpites += 1
    if palpite > 10:
        print('SOMENTE NÚMEROS DE 0 A 10!!!!')
        palpite = int(input('Qual é seu palpite?: '))
    else:
        if palpite < computador:
            print('Mais... tente mais uma vez.')
            palpite = int(input('Qual é seu palpite?: '))
        if palpite > computador:
            print('Menos... tente mais uma vez.')
            palpite = int(input('Qual é seu palpite?: '))
print('Você Acertou com {} tentativas!!!'.format(npalpites))