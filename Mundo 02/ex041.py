ano = int(input('Ano de nascimento: '))
idade = 2020 - ano

if idade <= 9:
    print('Até 9 anos: \033[1;34mMIRIM\033[m')
elif idade > 9 and idade <= 14:
    print('Até 14 anos: \033[1;34mINFANTIL\033[m')
elif idade > 14 and idade <= 19:
    print('Até 19 anos: \033[1;34mJUNIOR\033[m')
elif idade == 20:
    print('Até 20 anos: \033[1;34mSÊNIOR\033[m')
else:
    print('Acima: \033[1;34mMASTER\033[m')
