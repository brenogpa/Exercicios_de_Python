contador = soma = média = maior = menor = 0
num = int(input('Digite um número inteiro: '))
choice = str(input('Quer continuar? [S/N]: ')).strip().upper()[0]
while choice != 'N':
    if choice != 'S':
        print('\033[1;31mERRO\033[m')
        choice = str(input('Quer continuar? [S/N]: ')).strip().upper()[0]
    else:
        soma += num
        contador += 1
        num = int(input('Digite um número inteiro: '))
        choice = str(input('Quer continuar? [S/N]: ')).strip().upper()[0]
        if contador == 1:
            maior = menor = num
        else:
            if num > maior:
                maior = num
            elif num < menor:
                menor = num
média = soma / contador
print('Média dos valores: {:.2f}'.format(média))
print('Maior valor: {}\nMenor valor: {}'.format(maior,menor))