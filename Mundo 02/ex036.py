print('----EMPRÉSTIMO BANCÁRIO----')

casa = float(input('Valor da casa: R$'))
salário = float(input('Salário do comprador: R$'))
tempo = int(input('Quantos anos de financiamento?: '))
prestação = casa / (tempo * 12)
porcentagem = salário * 30 / 100

print('Para pagar uma casa de R${:.2f} em {} anos, a prestação será de R${:.2f}!'.format(casa, tempo, prestação))

if prestação <= porcentagem:
    print('Empréstimo pode ser CONCEDIDO!')
else:
    print('Empréstimo NEGADO')