nota1 = float(input('Nota 1: '))
nota2 = float(input('Nota 2: '))
média = (nota1 + nota2) / 2

if média < 5.0:
    print('Média: {:.1f} - \033[1;31mREPROVADO\033[m'.format(média))
elif média >= 5.0 and média <= 6.9:
    print('Média: {:.1f} - \033[1;33mRECUPERAÇÃO\033[m'.format(média))
else:
    print('Média: {:.1f} - \033[1;34mAPROVADO\033[m'.format(média))
