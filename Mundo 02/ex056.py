soma = 0
média = 0
velho = 0
nomevelho = ''
mulheres = 0
for p in range(1,5):
    print('----- {}ª PESSOA -----'.format(p))
    nome = str(input('Nome: ')).title().strip()
    idade = int(input('Idade: '))
    sexo = str(input('Sexo [M/F/O]: ')).capitalize().strip()
    soma += idade
    if idade > velho and sexo == 'M':
        velho = idade
        nomevelho = nome
    if idade < 20 and sexo == 'F':
        mulheres += 1
    if sexo != 'M' and sexo != 'F' and sexo != 'O':
        print('\033[1;31mERRO\033[m - As opções válidas são "M", "F" ou "O".')
média = soma / p
print('A média da idade do grupo é de {:.1f} anos'.format(média))
if velho > 0 and nomevelho != '':
    print('O homem mais velho tem {} anos e se chama {}.'.format(velho, nomevelho))
if mulheres > 0:
    print('Ao todo são {} mulheres com menos de 20 anos.'.format(mulheres))