ano = int(input('Informe seu ano de nascimento: '))

if ano > 2002:
    print('Você deverá se alistar no serviço militar em \033[0;31m{}\033[m anos!'.format(ano - 2002))
elif ano == 2002:
    print('Está na \033[0;31mhora de se alistar!!!\033[m')
else:
    print('Você deveria ter se alistado à \033[0;31m{}\033[m anos!'.format(2002 - ano))
