count = total = 0
choice = 's'
lista_nomes = []
lista_preco = []

while choice == 's':
    produto = str(input('Nome do Produto: ')).capitalize()
    preco = float(input('Preço: R$ '))
    if preco >= 1000:
        count += 1
    total += preco

    lista_nomes.append(produto)
    lista_preco.append(preco)

    while True:
        choice = str(input('Quer continuar?: ')).strip().lower()
        if choice in 'sn':
            break

nome = lista_nomes[0]
menor = lista_preco[0]

for x in range(len(lista_preco)):
    if lista_preco[x] < menor:
        menor = lista_preco[x]
        nome = lista_nomes[x]

print(f'Preço total: R${total}')
print(f'{count} produtos de mais de R$ 1000,00')
# print(len([a for a in lista_preco if a >= 1000]))
print('Produto mais barato: %s - R$%.2f' % (nome, menor))
#print('%.0f'%(sum(lista_preco)))