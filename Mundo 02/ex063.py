print('-'*22)
print('Sequência de Fibonacci')
print('-'*22)
n = int(input('Quantos termos você quer mostrar?: '))
n1 = 0
n2 = 1
contador = 3
print('{} → {} '.format(n1,n2),end='')
while contador <= n:
    n3 = n1 + n2
    print('→ {} '.format(n3),end='')
    n1 = n2
    n2 = n3
    contador += 1