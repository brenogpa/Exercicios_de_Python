game = [' '] * 9
player = 'O'
active = True


def print_board(board):
    print('\n' * 100)
    print(
        f'{board[6]}|{board[7]}|{board[8]}\n-+-+-\n{board[3]}|{board[4]}|{board[5]}\n-+-+-\n{board[0]}|{board[1]}|{board[2]}\n')


def make_play(board, player, choice):
    if choice > 9:
        return True

    if board[choice - 1] == ' ':
        board[choice - 1] = player
        return False

    return True


def won_game(board):
    def check(a, b, c):
        return a == b == c != ' '

    if check(board[6], board[7], board[8]):
        return board[6]
    if check(board[3], board[4], board[5]):
        return board[3]
    if check(board[0], board[1], board[2]):
        return board[0]
    if check(board[6], board[3], board[0]):
        return board[6]
    if check(board[7], board[4], board[1]):
        return board[7]
    if check(board[8], board[5], board[2]):
        return board[8]
    if check(board[4], board[5], board[0]):
        return board[4]
    if check(board[6], board[4], board[2]):
        return board[8]

    return False


def game_ended(board):
    return len([a for a in board if a == ' ']) == 0


while active:
    print_board(game)

    while make_play(game, player, int(input(f'Vez de {player}:'))):
        pass

    player = 'O' if player == 'X' else 'X'

    active = not game_ended(game) and not won_game(game)

    if not active:
        print_board(game)
        winner = won_game(game)

        if winner:
            print(f'{winner} ganhou')

        replay = input('Jogar de novo?')

        if replay in 'sSyY':
            game = [' '] * 9
            player = 'O'
            active = True
