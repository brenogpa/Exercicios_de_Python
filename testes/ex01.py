def checkio(data: str) -> bool:
    n_count = 0
    u_count = 0
    l_count = 0
    r = False

    check = data
    if len(check) >= 10:
        for char in check:
            if char in '1234567890':
                n_count += 1
            if char in 'QWERTYUIOPASDFGHJKLZXCVBNM':
                u_count += 1
            if char in 'qwertyuiopasdfghjklzxcvbnm':
                l_count += 1

        if n_count > 0:
            if u_count > 0:
                if l_count > 0:
                    r = True

    return r