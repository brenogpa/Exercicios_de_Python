def time_converter(time):
    h, m = map(int, time.split(':'))
    print(f"{(h-1)%12+1}:{m:02d} {'ap'[h>11]}.m.")


time_converter('12:30') == '12:30 p.m.'
time_converter('09:00') == '9:00 a.m.'