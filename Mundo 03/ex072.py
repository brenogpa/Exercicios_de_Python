contagem = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco',
            'seis', 'sete', 'oito', 'nove', 'dez', 'onze',
            'doze', 'treze', 'catorze', 'quinze', 'dezesseis',
            'dezessete', 'dezoito', 'dezenove', 'vinte')

while True:
    num = int(input('Digite um número de 0 a 20: '))

    if 0 <= num <= 20:
        print('O número digitado foi o {}'.format(contagem[num]))
        choice = str(input('Quer continuar? [S/N]: ')).upper()
        while choice not in 'SN':
            print('\033[1;31mResposta Inválida\033[m -', end=' ')
            choice = str(input('Quer continuar? [S/N]: ')).upper()
        if choice == 'N':
            break
    else:
        print('Tente novamente\n ')