num = [2, 5, 9, 1]
num[2] = 3 #troca o valor da posição 2 pelo valor 3 (9 vira 3)

num.append(7) #adicionar o valor 7 ao final da lista

num.insert(2, 0) #adiciona o número 0 na posição 2

num.pop(2) #deleta o item da posição 2

num.remove(2) #remove o primeiro valor 2 que encontrar

num.sort() #coloca em ordem

num.sort(reverse=True) #coloca em ordem inversa

print(len(num)) #conta quantos elementos tem na lista

print(num)

a = [2, 3, 4, 7]

b = a #b é uma lista igual a lista a
b[2]= 8 #vai mudar as duas listas pq são iguais

b= a[:] #b fez uma cópia dos valores de a
b[2]= 8# vai mudar apenas na b pois é uma cópia de a


