times = ('Corinthians', 'Palmeiras', 'Santos', 'Grêmio', 'Cruzeiro',
         'Flamengo', 'Vasco', 'Chapecoense', 'Atlético', 'Botafogo',
         'Atlético-PR', 'Bahia', 'São Paulo', 'Fluminense', 'Sport Recife',
         'EC Vitória', 'Coritiba', 'Avaí', 'Ponte Preta', 'Atlético-GO')

a = '-=' * 15

print(f'{a}\nLista de times do Brasileirão: {times}\n'
     
      f'{a}\nOs 5 primeiros são: {times[0:5]}\n'
     
      f'{a}\nOs 4 últimos são: {times[-4:]}\n'
     
      f'{a}\nTimes em ordem alfabética: {sorted(times)}\n'
      
      f'{a}\nO Chapecoense está na {times.index("Chapecoense")+1}ª posição')