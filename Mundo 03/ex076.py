produtos = ('Lápis', 1.75,
            'Borracha', 2,
            'Caderno', 15.90,
            'Estojo', 25,
            'Transferidor', 4.20,
            'Comprasso', 9.99,
            'Mochila', 120.32,
            'Canetas', 22.30,
            'Livro', 34.90)

print('-'*40)
print(f'{"TABELA DE PREÇOS":^40}')
print('-'*40)

for posicao in range(len(produtos)):
    if posicao % 2 == 0:
        print(f'{produtos[posicao]:.<30}',end='')
    else:
        print(f'R${produtos[posicao]:>7.2f}')

print('-'*40)