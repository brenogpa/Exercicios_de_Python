numeros = []
pares = []
impares = []
while True:
    num = int(input('Insira o valor: '))
    numeros.append(num)
    if num % 2 == 0:
        pares.append(num)
    else:
        impares.append(num)
    choice = str(input('Quer continuar?(s/n):')).lower()

    if choice != 's' and choice != 'n':

        while choice != 's' and choice != 'n':
            print('\033[1;31mResposta Inválida\033[m')
            choice = str(input('Quer continuar?(s/n): ')).lower()

    if choice == 'n':
        break

print(f'A lista completa é {numeros}\n'
      f'A lista de pares é {pares}\n'
      f'A lista de ímpares é {impares}')
