a = (2, 5, 4)
b = (5, 8, 1, 2)
c = a + b
print(c)


print(len(c))       #quantos elementos tem na tupla

print(c.count(5))       # quantas vezes aparece o '5' na tupla

print(c.index(8))       #em que posição está o '8' na tupla
print(c.index(2, 5))

pessoa = ('Gustavo', 39, 'M', 99.88)
print(pessoa)
del(pessoa)     #apaga a tupla