valores = []
loop = 's'

while loop == 's':

    valor = input('Insira um valor: ')
    if valor not in valores:
        valores.append(valor)
        print('Valor adicionado com sucesso!')
        loop = str(input('Deseja continuar? (s/n): ')).lower()
    else:
        print('Escolha um valor diferente!')

    while loop != 's' and loop != 'n':
        loop = str(input(('Digite uma resposta \033[1;31mVÁLIDA\033[m!: '))).lower()

print(sorted(valores))
