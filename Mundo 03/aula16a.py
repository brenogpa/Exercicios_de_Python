#TUPLAS são imutáveis

lanche = ('Hambúrguer', 'Suco', 'Pizza', 'Pudim', 'Batata Frita')

#1
for comida in lanche:
    print(f'Eu vou comer {comida}')

print('-' * 30)

#2
for cont in range(0, len(lanche)):
    print(f'Eu vou comer {lanche[cont]} na posição {cont}')

print('-' * 30)

#3
for posicao, comida in enumerate(lanche):
    print(f'Eu vou comer {comida} na posição {posicao}')

print('-' * 30)

#4
print(len(lanche))

print('-' * 30)

#5
print(sorted(lanche))   #organiza