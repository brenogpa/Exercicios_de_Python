pessoas = []
dados = []
maior = menor = 0

while True:

    dados.append(str(input('Nome: ')).title())
    dados.append(float(input('Peso: ')))

    if len(pessoas) == 0:
        maior = menor = dados[1]
    else:
        if dados[1] > maior:
            maior = dados[1]
        if dados[1] < menor:
            menor = dados[1]

    pessoas.append(dados[:])
    dados.clear()

    choice = str(input('Quer continuar? (S/N): ')).upper()

    if choice != 'N' and choice != 'S':
        choice = str(input('Quer continuar? (S/N): ')).upper()
    if choice == 'N':
        break

print(f'Ao todo, você cadastrou {len(pessoas)} pessoa(s).')

print('O maior peso foi de ', end='')
for p in pessoas:
    if p[1] == maior:
        print(f'{p[0]}, {maior} Kg.')

print(f'O menor peso foi de ', end='')
for p in pessoas:
    if p[1] == menor:
        print(f'{p[0]}, {menor} Kg.')
