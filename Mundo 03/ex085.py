imp = []
par = []

for n in range(1, 8):
    num = int(input(f'Digite o {n} valor: '))
    if num % 2 == 0:
        par.append(num)
    else:
        imp.append(num)
print(f'Os valores pares digitados foram: {sorted(par)}\n'
      f'Os valores ímpares digitador foram: {sorted(imp)}')
