valores = []

while True:
    valor = int(input('Digite um valor: '))
    valores.append(valor)
    choice = str(input('Quer continuar?(s/n): ')).lower()

    if choice != 's' and choice != 'n':

        while choice != 's' and choice != 'n':
            print('\033[1;31mResposta Inválida\033[m')
            choice = str(input('Quer continuar?(s/n): ')).lower()

    if choice == 'n':
        break
valores.sort(reverse=True)

print(f'Você digitou {len(valores)} valores.\n'
      f'Os valores em ordem decrescente são {valores}')

if 5 in valores:
    print('O valor 5 faz parte da lista')
else:
    print('O valor 5 não faz parte da lista')
