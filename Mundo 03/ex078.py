valores = []
maior = 0
menor = 0

for n in range(0, 5):
    valores.append(int(input(f'Insira o {n+1}º valor: ')))
    if n == 0:
        maior = menor = valores[n]
    else:
        if valores[n] > maior:
            maior = valores[n]
        if valores[n] < menor:
            menor = valores[n]

for posicao, valor in enumerate(valores):
    if valor == maior:
        print(f'Maior valor foi o {maior} na {posicao+1}ª posição.')
    if valor == menor:
        print(f'Menor valor foi o {menor} na {posicao + 1}ª posição.')
